<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class OperadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operadores')->insert([
            'ci'=>'6442850',
            'paterno'=>'Catalan',
            'materno'=>'Mendez',
            'nombres'=>'Tania',
            'fecha_nacimiento'=>'2015-06-19',
            'telefono'=>'12345'
        ]);

        DB::table('operadores')->insert([
            'ci'=>'6442856',
            'paterno'=>'Campos',
            'materno'=>'Perez',
            'nombres'=>'Dorys',
            'fecha_nacimiento'=>'2015-06-20',
            'telefono'=>'12345'
        ]);

        DB::table('operadores')->insert([
            'ci'=>'6442857',
            'paterno'=>'Donaire',
            'materno'=>'Rodriguez',
            'nombres'=>'Julissa',
            'fecha_nacimiento'=>'2015-08-30',
            'telefono'=>'12345'
        ]);

        DB::table('operadores')->insert([
            'ci'=>'6442858',
            'paterno'=>'Merida',
            'materno'=>'Escobar',
            'nombres'=>'Andres',
            'fecha_nacimiento'=>'2015-11-30',
            'telefono'=>'12345'
        ]);

        DB::table('operadores')->insert([
            'ci'=>'6442859',
            'paterno'=>'Camacho',
            'materno'=>'Mendez',
            'nombres'=>'Pablo',
            'fecha_nacimiento'=>'2015-06-25',
            'telefono'=>'12345'
        ]);
    }
}
