<?php

namespace App\Http\Controllers;

use App\Operadores;
use Illuminate\Http\Request;

class OperadoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operadores = Operadores::all();
        return $operadores;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operador = Operadores::create($request->all());
        return $operador;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operadores  $operador_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $operador_id)
    {
        $operador = Operadores::find($operador_id);
        $operador->ci = $request->input('ci');
        $operador->paterno = $request->input('paterno');
        $operador->materno = $request->input('materno');
        $operador->nombres = $request->input('nombres');
        $operador->fecha_nacimiento = $request->input('fecha_nacimiento');
        $operador->telefono = $request->input('telefono');
        $operador->save();
        echo json_encode($operador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operadores  $operador_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($operador_id)
    {
        $operador = Operadores::find($operador_id);
        $operador->delete();
    }
}
