<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operadores extends Model
{
    protected $fillable = ["ci","paterno","materno", "nombres","fecha_nacimiento","telefono"];
}
